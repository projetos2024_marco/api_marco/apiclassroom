const cron = require("node-cron");
const cleanUpSchedules = require("./services/cleanUpSchedulesServices");

// agendamento da limpeza

cron.schedule("*/30 * * * * *", async()=>{
    try {
        await cleanUpSchedules();
        console.log("Limpeza automática executada")
    } catch (error){
        console.log("erro ao executar limpeza automática")
    }
})