const connect = require("../db/connect");

async function cleanUpSchedules(){
    const currentDate = new Date();

    currentDate.setDate(currentDate.getDate() - 7)

    const formattedDate = currentDate.toISOString().split("T")[0];

    const query = `DELETE FROM schedule WHERE dateEnd < ?`
    const values = [formattedDate];

    return new Promise((resolve, reject)=>{
        connect.query(query, values, function(err, result){
            if(err){
                console.error("Erro Mysql", err);
                return reject(new Error("Erro ao limpar agendamentos"))
            }
            console.log("Agendamentos antigos apagados");
            resolve("Agendamentos antigos limpos com sucesso");
        })
    })
}
module.exports = cleanUpSchedules;